##0 ______________________________________________
##0
##3         .:: PONTE DAS FADAS ::.
##0
##0 A quest da ponte de fadas foi consertado. Vcs
##0 ja podem testar sua bravura pulando da ponte.
##0 ______________________________________________
##0
##3         .:: Precisamos de Você! ::.
##0
##0 Contribua com o The Mana World Brasil. Faça 
##0 sua doação e ajude a manter o nosso servidor 
##0 vivo. Para doar acesse: ##3http://goo.gl/uKjj9
##0 ______________________________________________
##0
##3          .:: Mensagem de IRC ::.
##0
##0 Estamos instalando um mensageiro instantaneo 
##0 de IRC diretamente no site oficial do TMW-BR.
##0 Quem precisar de ajuda de fora do jogo, favor
##0 se conectar ao canal [##3 #TMW-BR ##0]no servidor
##0 [##3 irc.freenode.net ##0].
##0 ______________________________________________
##0
##3          .:: Pedido de Socorro ::.
##0
##0 Se alguém ficar preço, peça ajuda a um GM pelo
##0 comando: ##2 @wgm socorro!
##0 ______________________________________________
##0
##3          .:: REGRAS DO SERVIDOR ::.
##0
##1 Quer Novidade? ##0→ Se querem novidade no TMW-BR 
##0 façam vocês mesmos. Lembre-se que o TMW-BR é 
##0 100% feito pelos próprios jogadores. Por isso, 
##0 recrutamos ##2Mapers, Pixelarts, Questarts, 
##2 Publishers e Treinadores.
##0
##0 Envie seus trabalhos para:
##0  * ##3 http://forums.themanaworld.com.br
##0 Envie suas dúvidas para:
##0  * ##3 Lunovox<rui.gravata@gmail.com>
##0 
##1 Banco de Dado Mutável ##0→ A administração 
##0 reservamos o direito de alterar os atributos de 
##0 qualquer item que esteja no servidor sem prévia 
##0 comunicação a quem quer que seja. Esta medida 
##0 será tomanada para balancear os poderes de alguns 
##0 jogadores que estão semi-indestrutíveis.
##0
##1 Não use softbots! ##0→ É proibido usar qualquer 
##0 software que facilite o desenvolvimento do jogador. 
##0 Se o GM falar com o jogador que está se movendo, 
##0 o jogador deve responder. Mas se o char estiver 
##0 parado por causa do afastamento do jogador ao 
##0 teclado, o GM não pode exigir resposta. ##2Pois o
##2 char está parado porque o jogador longe do teclado.
##0
##1 Respeite a todos! ##0→ Respeite até aqueles que te 
##0 agrediram. ##2Trolls serão penalizados!
##0 
##1 Nunca empreste contas! ##0→ É proibido emprestar 
##0 contas. Se quem solicitou empréstimo de conta
##0 fizer alguma infração de regras, quem permitiu 
##0 tal empréstimo será punido no lugar de quem 
##0 solicitou o emprestimo.
##0 
##1 Evite emprestar ítens! ##0→ Não existem empréstimos 
##0 em MMORPGs. Quem pegou emprestado algum item, 
##0 não tem nenhum obrigação de devolver.
##0 
##1 Mendigância perniciosa! ##0→ Não insista pedindo 
##0 itens ou favores aos GMs, DEVs, ADMs ou outros 
##0 players. Itens são conseguidos por quest ou por 
##0 eventos sazonais.
##0 
##3 Façamos um servidor agradável para todos! =D
##0 ______________________________________________
##0
##0 ASS.: ##2Administrador Lunovox Heavenfinder
