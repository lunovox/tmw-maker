#labels Repositório,TMW,MMORPG,RPG,Jogo,Game,TheManaWorld,TMW-Maker
==Sistema → Configuração(Ctrl+G) → Conexão → Repositório==

É o endereço onde fica o Repositório(sistema na internet) do tipo Subversion (também conhecido como "SVN") do servidor de The Mana World. Normalmente é divido em apenas 2 pastas: "eathena-data", "tmwdata". Por padrão, já vem preenchido o campo "Repositório"  com o endereço do repositório SVN do TMW-BR "http://themanaworld-br.googlecode.com/svn/trunk", mas você pode escolher o endereço do seu repositório SVN preferido. Os mais experientes conseguem criar seu próprio repositório SVN a partir de cópias de outros repositórios SVNs.

===Tipos de Configurações de Conexão SVN===

 * *HTTPS* - Se endereço do repositório SVN mostrado na foto abaixo começar com "*https*" (com "s") então o TMW-Maker pedirá uma identificação com *Nome de Usuário* e *Senha* que, se forem fornecidas corretamente, darão o poder de re-enviar as alterações do jogo de volta para o repositório na internet, para que todos vejam o que foi feito pelo usuário do TMW-Maker. Isso permite que vários usuários de locais diferentes do planeta possas editar um único jogo. (Ativa a colaboração multi-usuário) || *NOTA:* Se você desejar colaborar com o desenvolvimento do TMW-BR através do TMW-Maker, solicite o "*Nome de Usuário*" e "*Senha*" do repostório svn do TMW-BR ao administrador [http://code.google.com/u/diogorbg/ Diogo_RBG]. ||

 * *HTTP* - Se endereço do repositório SVN mostrado na foto abaixo começar com "*http*" (sem "s") então o TMW-Maker NÃO pedirá nenhuma uma identificação. Possibilitando apenas que o usuário baixe um cópia somente leitura do repositório para o computador do usuário do TMW-Maker. Ou seja, você usará e modificará o jogo sem precisa da intenet, mas não conseguirá compartilhar suas alterações no jogo e nem conseguirá receber ajudar de outros usuários, via internet pelo TMM-Maker. (Sem a colaboração múltiplos usuários)

https://tmw-maker.googlecode.com/svn/Imagens/Componentes/FrmConfiguracaoConexao.png

|| *NOTA:* Futuramente faremos o TMW-Maker se adequar ao idioma padrão do sistema do usuário. Quando isso ocorrer colocaremos os repositórios SVNs oficias de cada pais que possuir um servidor The Mana World. ||