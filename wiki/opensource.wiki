#labels OpenSource,TMW-Maker,SoftwareGrátis,ProgramaGrátis
= Licença tipo Open Source =

*Open Sources* são licenças que permite gratuitamente baixar, instalar, usar, estudar e redistribuir sem precisar dar satisfação a ninguém. Também é permitido gratuitamente modificar o código fonte do programa e redistribui-lo novamente, evolui-lo conforme suas habilidade, desde que não seja retirado o "*Nome do Projeto*" e nem o seus "*Desenvolvedores Precedentes*".

Licenças Open Source não significam que a ferramenta não pode ser comercializadas. Significa apenas que seus comerciantes não possuem nenhum direto de exclusividade sobre a ferramenta. Isso dá ao usuário do TMW-Maker o livre poder de decisão do que fazer com esta ferramenta, desde que não prejudique outros usuário.

<p align="center"><img src="http://harmoniaecontraponto.files.wordpress.com/2009/10/opensource_image.jpg">

Mais detalhes: [http://pt.wikipedia.org/wiki/Open_source]